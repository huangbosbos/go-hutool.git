package main

import (
	"bytes"
	"gitee.com/huangbosbos/go-hutool/go-hutool-log/main/cn/gohutool/log"
	"github.com/json-iterator/go"
	"io"
	"net/http"
	"time"
)

func main() {
	path := "https://pigx.pig4cloud.com/admin/menu"
	list := Get(path)
	_byte := []byte(list)
	code_data := jsoniter.Get(_byte, "code").ToString()
	json_data := jsoniter.Get(_byte, "data").ToString()
	log.INFO.Println("获取第一个", code_data)
	log.INFO.Println("获取第二个", json_data)

	////获取第一个
	//size := json_data.Size()
	//_data := []byte(json_data.ToString())
	//for i := 0 ; i< size ; i++{
	//	balanceAmount := jsoniter.Get(_data,i,"balanceAmount").ToString()
	//	fmt.Println(balanceAmount)
	//}
}

//get请求
func Get(url string) (response string) {
	client := http.Client{Timeout: 5 * time.Second}
	resp, error := client.Get(url)
	defer resp.Body.Close()
	if error != nil {
		panic(error)
	}

	var buffer [512]byte
	result := bytes.NewBuffer(nil)
	for {
		n, err := resp.Body.Read(buffer[0:])
		result.Write(buffer[0:n])
		if err != nil && err == io.EOF {
			break
		} else if err != nil {
			panic(err)
		}
	}

	response = result.String()
	return
}
